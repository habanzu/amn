import numpy as np


def QRAlgorithm(A):
    """Computes the Eigenvalues of the given matrix and returns them."""
    # TODO: Implement the function
    return np.zeros(A.shape[0])


if __name__=="__main__":
    A = np.random.rand(5,5)
    A += A.T
    eval1 = sorted(QRAlgorithm(A))
    eval2 = sorted(sorted(np.linalg.eigh(A)[0]))
    print('Eigenvalues computed by QRAlgorithm():')
    print(eval1)
    print('Eigenvalues computed by numpy.linalg.eigh():')
    print(eval2)